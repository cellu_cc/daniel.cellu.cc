---
layout: default.liquid

title: Recipebook Notes, Part 1
is_draft: true
tags: [thought]
description: thinking about recipes, and building a "capsule kitchen"
---

<script src="https://d3js.org/d3.v6.min.js"></script>
<script src="/scripts/force_directed_graph.js"></script>


## What is a recipe? 
A recipe is composed of three major attributes:

  * *Equipment:* the things like pots and pans that you'll need to cook the food
  * *Ingredients:* the food that becomes the meal
  * *Steps:* the instructions that describe how to use the *Equipment* to create the meal from the *Ingredients*

## The Capsule Pantry
Recipebook is a tool that helps you generate a *capsule pantry*.
The *capsule pantry* is a riff on the *capsule wardrobe*, which is essentially a set of clothes that you can wear reliably every day.
There are a few places that have explored this idea by proposing a set of ingredients that are generally useful in a wide variety of cuisines.

Recipebook, however, is working off of this hypothesis:

 > A *capsule pantry* is more about a set of common recipes than it is about a set of common ingredients

A tool that helps create a capsule pantry is therefore one that helps find a set of recipes with

### How to generate a capsule pantry
Recipes and Ingredients are nodes in a graph. 
Recipes can be connected 
Take an ingredient list for a Simple Salad listed below:

    SIMPLE SALAD
      HARD-BOILED EGG
      VINAIGRETTE
      Baby Arugula
      Cherry Tomatoes
      Parmesean Cheese

Everything in ALL CAPS is a recipe.
Note that a recipe can contain both ingredients and other recipes.
Represented as a graph, this would look like:

<!-- Load d3.js -->
<script type="text/javascript" src="/media/recipebook-capsule-salad.js"></script>
<script src="/scripts/force_directed_graph.js"></script>
<!-- Create a div where the graph will take place -->
<div class="card">
<div class="card-image">
<svg id="salad_graph"></svg>
<script>
chart(d3, "#salad_graph", salad_data, 500, 200);
</script>
</div>
  <div class="card-content">
    <div class="content">
      <b>Figure 1:</b> Simple salad example
    </div>
  </div>
</div>

<script type="text/javascript" src="/media/recipebook-capsule-allrecipes.js"></script>
<!-- Create a div where the graph will take place -->
<svg id="recipe_network"></svg>

<div class ="container">
<script>
chart(d3, "#recipe_network", plot_data, 500, 500);
</script>
</div>


Of these three, the *ingredients* are the hardest part of cooking.
*Ingredients* are a headache. 
They spoil, there's usually a bunch of ingredient left over, or there is too little and you have to get more. 
Several existing services try to address this issue by delivering meal kits. 
A big part of their value proposition is that you just get the ingredients for the thing you're cooking, so you dont have to worry about all of the problems listed above. 

Michelle and I prefer to work with our own recipes, and we typically like to get ingredients from some of the local suppliers around us. 
Good cheese from the local cheesemonger, good meat from the local butcher, etc. 
But the act of doing meal planning is a pain- it's nice to get staples, but trying to plan every week the ingredients for multiple meals is time-consuming and wasteful.


## What is the goal? 
The goal of Recipebook is, first, to provide a clear interface for storing recipes. 
Second, Recipebook uses 


## Schema set up

A recipe

    Recipe {
        ...
        title: String
        ingredients: [IngredientLink]
    }

An ingredient link

    IngredientLink {
        recipe: Recipe
        
    }
An ingredient link