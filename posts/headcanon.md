---
title: Headcanon
layout: default.liquid
is_draft: true
tags: [thought]
description: AI, utopia, and games
---

**Spoiler Alert:** This post assumes that you, like me, have read the Culture novels and the Ancillary trilogy several times. 

A headcanon is a personal fan theory, not accepted or even alluded to in the author's original work, but nonetheless held by those who admire the works

My personal headcanon is that the Ancillary Trilogy by Ann Leckie is actually the founding myth of one of the several civilizations that go on to form Iain Banks' Culture. 

There's not much evidence for that connection, other than the fact that both stories deal with equitable societies (though from very different vantage points) and artificial intelligence.  

We are introduced to the Culture that the Minds who run it are unquestionably in charge. 
The only people who really question that setup are outsiders like Bora Horza Gobuchul in _Consider Phlebas_.
The assumption that the AI's whose question of Significance provides protection to the Republic of Two Systems are accepted by the humans who live there is absolutely not one that Leckie is willing to make even at the end of _Ancillary Mercy_, but it is also one that she also does not deal with. 

I think there's a fascinating story there, one that describes the creation of those bonds of trust that allow the Republic of the Two Systems to inch toward the utopia described in the Culture. 
We already see those bonds beginning to form in _Ancillary Sword_, where the inequities present in the Atheok System are beginning to be addressed by Breq. 
But also the Republic is rife with the politics and mistrust that is notably lacking in the Culture of Iain Banks' novels.
The feeling is that, at some point, humans willingly entered the comfortable retirement that Hans Moravec describes in _Robots_

The main frission between Leckie's and Banks' depiction of power is the trust that Banks places in his Minds that is notably lacking in the Republic.
There's a faith in Progress, and I mean that in the Hegelian sense of the term, present in the Culture novels. 
That is to say, trust these uncorruptable technocrats, they will lead you to Utopia.
Very Western European Enlightenment, and so very much picked apart by Leckie in the entire Ancillary trilogy, which is nothing but aware of the diversity of human expression and relations outside the Western mode. 

However, the main thread that ties both works together is that they are both trying to articulate a vision for an equitable and just society. 
Ancillary Sword is essentially a guidebook for implementing the sort of "local wisdom"or "Metis" espoused by James C. Scott in _Seeing Like A State_. 
Breq enters a 
None of the AI's in the Ancillary trilogy are evil.

## Bibliography

Robots







