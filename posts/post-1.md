---
layout: default.liquid

title: Notes on Chiba City Blues
is_draft: true
tags: [thought]
description: Preparatory notes for the Chiba City Blues Project
---

Chiba City Blues is the opening chapter of Neuromancer, by William Gibson.