---
title: Umwelt
layout: default.liquid
is_draft: true
tags: [thought]
description: observations on subjective experience 2
---
Marcello, a friend of mine, is working on a show that involves oysters. 
We were having oysters for Michelle's birthday, and he said something along the lines of

> eating an oyster is one of the purest ways to experience a place

Marcello says stuff like that, that is why I like hanging out with him. 
It also got me thinking about an idea called Umwelt that I read about once, in an excellent book by Caroline O'Donnell called [Niche Tactics](https://www.goodreads.com/book/show/23504223-niche-tactics).

An organism's umwelt is its subjective understanding of the world.
These are the stimuli that become signs that then are translated into actions that, in turn, produce new stimuli. 

Eating an oyster is then an opportunity to experience its umwelt. 

It's especially relevant now because, for the first time in my life, I live with a pet.
That is to say, I live with an organism whose understanding of the world, their interpretation of the same experiences, is fundamentally different than mine. 
It is tempting, like Descartes (who wanted moral justification for eating meat) to fall into a solopsistic trap and write off that non-human experience as just a collection of instincts. 
That animal is then just an automaton that only appears "alive" but is actually no more alive than a machine assembled from (albeit quite) complex parts (and therefore eating it isn't immoral). 

Somehow that attitude percolated from Descartes into modern biology. 
When I was experiencing that one-two punch of of Darwinian Evolution and Molecular Biology back in college, it felt like there was very little barrier between matter and living things.  
That is, living things are just the same fundamental building blocks that have self-assembled in more and more complex configurations. 
They are whirlpools spinning off a paddle in the middle of a lake, just a churning group of atoms in a dynamic and ultimately temporary configuration that we mistake for a static thing called an organism.

Buried in that materialistic view of is a determinism from which I think umwelt offers an escape. 
Umwelt is primarly concerned with the world

I can think of no organism that more embodies this world view than an oyster.
The 

They are essentially machines, beholden to their genes and their environment for their survival. 
They have no agency.

Of course, the main problem with solopsism is, when does it stop? 
If animals only appear to be alive, then what makes humans, who are composed of the same parts and also evolved through natural selection, so different? 

## Area X


The thing about Darwinian Evolution is it is fundamentally impersonal. The view is pulled so far back that an individual organism's actions do not meaningfully contribute 

The concept of umwelt is posited

In many ways 
Umwelt was originally posited as an alternative to Darwinian evolution. 

I remember being struck