---
title: Umwelt
layout: default.liquid
is_draft: false
tags: [thought]
description: observations on subjective experience
---
Marcello, a friend of mine, is working on a show that involves oysters. 
We were having oysters for my partner's birthday (she loves oysters), and he said something along the lines of

> eating an oyster is one of the purest ways to experience a place

Marcello says things like this, which is one of the many reasons why I like hanging out with him. 
It got me thinking about how we think about living things.

The way I learned biology, living things are essentially dumb matter in more and more complex configurations.
This outlook traces back to Descartes, who wanted moral justification for eating meat and so argued that living things are just automata that only appear "alive" but are actually no more living than a machine assembled from (albeit quite) complex parts (and therefore eating them is not immoral). 

Granted, it is hard to look at the stunning complexity of molecular biology and Darwinian evolution and not fall into a solopsistic trap by writing off all non-human experience as just a collection of instincts programmed into some matter that learned how to self-replicate. 

From a distant enough view of life, this starts to make sense.
Look long enough in time, over the course of generations, and genes start to become the defining quality of an organism. 
Zoom far enough in, to the level of cells, and neurotransmitters and proteins start to seem like the true essence of matter, instead of the means.

## The Oyster

The oyster seems to be the perfect example of this mechanistic, Cartesian worldview.
Other than the larvae phase, where a fertilized egg will float through the water until it lands on a surface to attach to, the oyster's existence is fixed to a single place. 
It finds sustenance by filtering the seawater that flows through its open mouth for plankton and other organic particles. 
 
From this view, the oyster has no more agency than a vortex spinning out from a canoe paddle on the surface of a lake. 
It is the momentary arrangement of a few atoms into a shape we call "oyster", powered by the currents of the water which bring new materiel into this creature's mouth to be filtered out and incorporated. 
The only things that outlast this arrangement are the shell, made of calcium carbonate, and a record of its genes in its decendants.

## The Umwelt

Of course, living things are much more complex than just their matter and their genes. 
They respond to their environment, they adapt, they, within the confines of their physiology, change themselves in response to signals. 
While it might be over the course of several generations that natural selection will encourage traits that help organisms perform in a niche that they have filled, the essential cause of that specialization is a decision, however implicit, made by an organism in response to environmental stimuli to fill that niche even though it was not, initially, suited for it.

Von Uexhull, the creator of the concept of the umwelt, thought of these immediate decisions, made in a particular context, as "horizontal rules", in contrast to the "vertical lineages" provided by genes. 
The description of the world that provides the impetus for these decision, as sensed by the organism, is the umwelt of that organism. 
The umwelt is the subjective experience of the organism, the result of constant negotiation between itself and its environment, including other organisms.

> the space peculiar to each animal, wherever that animal may be, can be compared to a soap bubble which completely surrounds the creature at a greater or less distance. The extended soap bubble constitutes the limit of what is finite for the animal, and therewith the limit of its world; what lies behind that is hidden in infinity. 

Even the oyster navigates a complex world full of signals.
Scientists have found that the oysters of the _C. Gigas_ species will modulate the opening and closing of their shells according to the complex environmental stimuli they receive, the movement of the tides, the sun rising and setting.

## The Oyster's Umwelt

The Darwinian/Cartesian view of organisms and the concept of the Umwelt are not mutually exclusive; they are just two different ways of viewing a thing. 


