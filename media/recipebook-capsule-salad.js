salad_data = {
    "nodes": [
        { "id": "Hard-Boiled Egg", "group": 1 }
      , { "id": "Simple Salad", "group": 1 }
      , { "id": "Viniagrette", "group": 1 }
      , { "id": "Baby Arugula", "group": 2 }
      , { "id": "Cherry Tomatoes", "group": 2 }      
      , { "id": "Parmesean", "group": 2 }       
      ]
  , "links": [
        { "source": "Simple Salad", "target": "Baby Arugula", "value": 1 }
      , { "source": "Simple Salad", "target": "Cherry Tomatoes", "value": 1 }
      , { "source": "Simple Salad", "target": "Hard-Boiled Egg", "value": 1 }
      , { "source": "Simple Salad", "target": "Parmesean", "value": 1 }
      , { "source": "Simple Salad", "target": "Viniagrette", "value": 1 }
      ]
}
