plot_data = {
    "nodes": [
        { "id": "Carbonara", "group": 1 }
      , { "id": "Fresh Tomato Pasta", "group": 1 }
      , { "id": "Hard-Boiled Egg", "group": 1 }
      , { "id": "Simple Salad", "group": 1 }
      , { "id": "Viniagrette", "group": 1 }
      , { "id": "Baby Arugula", "group": 2 }
      , { "id": "Cherry Tomatoes", "group": 2 }     
      , { "id": "Eggs", "group": 2 }    
      , { "id": "Lemon", "group": 2 }    
      , { "id": "Olive Oil", "group": 2 }    
      , { "id": "Pancetta", "group": 2 }      
      , { "id": "Parmesean", "group": 2 }      
      , { "id": "Pecorino Romano", "group": 2 } 
      , { "id": "Pepper", "group": 2 }          
      , { "id": "Salt", "group": 2 }       
      , { "id": "Shallots", "group": 2 }      
      , { "id": "Spaghetti", "group": 2 }    
      ]
  , "links": [
        { "source": "Carbonara", "target": "Eggs", "value": 1 }
      , { "source": "Carbonara", "target": "Olive Oil", "value": 1 }
      , { "source": "Carbonara", "target": "Pancetta", "value": 1 }
      , { "source": "Carbonara", "target": "Parmesean", "value": 1 }
      , { "source": "Carbonara", "target": "Pecorino Romano", "value": 1 }
      , { "source": "Carbonara", "target": "Pepper", "value": 1 }
      , { "source": "Carbonara", "target": "Salt", "value": 1 }
      , { "source": "Carbonara", "target": "Spaghetti", "value": 1 }
      , { "source": "Fresh Tomato Pasta", "target": "Cherry Tomatoes", "value": 1 }
      , { "source": "Fresh Tomato Pasta", "target": "Olive Oil", "value": 1 }
      , { "source": "Fresh Tomato Pasta", "target": "Parmesean", "value": 1 }
      , { "source": "Fresh Tomato Pasta", "target": "Pecorino Romano", "value": 1 }
      , { "source": "Fresh Tomato Pasta", "target": "Cherry Tomatoes", "value": 1 }
      , { "source": "Fresh Tomato Pasta", "target": "Pepper", "value": 1 }
      , { "source": "Fresh Tomato Pasta", "target": "Salt", "value": 1 }
      , { "source": "Fresh Tomato Pasta", "target": "Spaghetti", "value": 1 }
      , { "source": "Hard-Boiled Egg", "target": "Eggs", "value": 1 }
      , { "source": "Hard-Boiled Egg", "target": "Salt", "value": 1 }
      , { "source": "Simple Salad", "target": "Baby Arugula", "value": 1 }
      , { "source": "Simple Salad", "target": "Cherry Tomatoes", "value": 1 }
      , { "source": "Simple Salad", "target": "Hard-Boiled Egg", "value": 1 }
      , { "source": "Simple Salad", "target": "Parmesean", "value": 1 }
      , { "source": "Simple Salad", "target": "Viniagrette", "value": 1 }
      , { "source": "Viniagrette", "target": "Lemon", "value": 1 }
      , { "source": "Viniagrette", "target": "Olive Oil", "value": 1 }
      , { "source": "Viniagrette", "target": "Pepper", "value": 1 }
      , { "source": "Viniagrette", "target": "Salt", "value": 1 }
      , { "source": "Viniagrette", "target": "Shallots", "value": 1 }
      ]
}
