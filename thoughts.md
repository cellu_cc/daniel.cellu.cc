---
title: Daniel's Thoughts 
description: (Those Written Down)

layout: default.liquid
---

{% for post in collections.posts.pages %}
{% for tag in post.tags %}
{% if tag == "thought"%}
<h4 class="title mt-3" >
    <a href="{{ post.permalink }}"> 
        {{ post.title }}
    </a>
</h4>
<h6 class="subtitle is-italic">
{{ post.description }}
</h6>
{% endif %}
{% endfor %}
{% endfor %}

