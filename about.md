---
title: About Daniel
layout: default.liquid
---
<figure class="image px-6">
<img src="/media/daniel.golden.record.png">
</figure>
My name is Daniel Cellucci. I am a human being who sometimes rides bikes, sometimes bakes bread, and sometimes helps explore space in the Intelligent Systems Division at NASA Ames Research Center. I live in rainy Portland, Oregon with my partner, Michelle, and a cat we call Mio. We aren't sure what she calls herself.

### About this blog

This blog is a place where I put my thoughts and document some of my projects. It is a static site generated from [Cobalt](https://github.com/cobalt-org/cobalt.rs), styled using [Bulma](https://bulma.io/), and hosted on [Netlify](https://www.netlify.com/). The font is [Source Serif Pro](https://en.wikipedia.org/wiki/Source_Serif_Pro).